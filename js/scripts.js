jQuery(document).ready(function() {

	var item = $('.nav-pills>li');

	item.click(function(e) {

		if ( item.hasClass('active') ) {

			console.log(item.hasClass('active'));
			$(this).addClass('caret-down');
		} 
		else {
			console.log(item.hasClass('active'));
			item.removeClass('caret-down')

		}

	});

	var showListDetails = $('.detail-list');
	var detailBtn = $( '#compareBenefit' );

	$('.popular').removeClass('h-545');

	detailBtn.click(function() {
		detailBtn.text('Compare Benefits');
		showListDetails.toggle('slow', function() {
			$('.popular').addClass('h-545');
		});
		detailBtn.text('Close');
	});


	$('.owl-carousel').owlCarousel({
	    loop:true,
	    margin:10,
	    lazyLoad:true,
	    responsiveClass:true,
	    responsive:{
	        0:{
	            items:2,
	            nav:false,
	        },
	        600:{
	            items:4,
	            nav:false
	        },
	        768:{
	            items:5,
	            nav:false
	        },
	        1000:{
	            items:5,
	            nav:false,
	            loop:false
	        }
	    }
	});
});